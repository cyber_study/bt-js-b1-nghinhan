// BÀI 1
/**
 * Đầu vào: số ngày = n
 *
 * Xử lí :
 *  khởi tạo n;
 *  khởi tạo KẾT QUẢ;
 *  KẾT QUẢ = 100k * n;
 *  in ra KQ
 *
 * Đầu ra: Lương = 100,000 * n
 */

function calculateSalary() {
  let workDay;
  workDay = document.getElementById("ngaycong").value;
  let result_1 = document.getElementById("result_1");
  result_1.innerText = workDay * 100000;
}

// BÀI 2
/**
 * Đầu vào: nhập n[1..5]
 *
 * Xử lí :
 *  khởi tạo n1..n5;
 *  khởi tạo KẾT QUẢ;
 *  KẾT QUẢ = (n1 + n2 + n3 + n4 + n5) / 5;
 *  in ra KQ
 *
 * Đầu ra: giá trị trung bình
 */

function cau2() {
  let num1 = document.getElementById("n1").value;
  let num2 = document.getElementById("n2").value;
  let num3 = document.getElementById("n3").value;
  let num4 = document.getElementById("n4").value;
  let num5 = document.getElementById("n5").value;
  let result_2 = document.getElementById("result_2");
  result_2.innerText =
    (num1 * 1 + num2 * 1 + num3 * 1 + num4 * 1 + num5 * 1) / 5;
}

// BÀI 3
/**
 * Đầu vào: nhập số tiền muốn Q đổi :"D
 *
 * Xử lí :
 *  khởi tạo usd
 *  khởi tạo KẾT QUẢ;
 *  KẾT QUẢ = usd * 23500;
 *  in ra KQ
 *
 * Đầu ra: số tiền
 */

function cau3() {
  let usd = document.getElementById("usd").value;
  let result_3 = document.getElementById("result_3");
  result_3.innerText = usd * 23500;
}

// BÀI 4
/**
 * Đầu vào: chieu dai, chieu ngang
 *
 * Xử lí :
 *  khởi tạo with, height;
 *  khởi tạo chuvi, dien tich;
 *  chuvi = (with + height)*2;
 *  dientich = with*height;
 *  in ra KQ;
 *
 * Đầu ra: chu vi, dien tich
 */

function cau4() {
  let w_Cau4 = document.getElementById("with-rec").value;
  let h_Cau4 = document.getElementById("height-rec").value;
  let chuvi = document.getElementById("result_4_perimeter");
  let dientich = document.getElementById("result_4_area");
  chuvi.innerText = (w_Cau4 * 1 + h_Cau4 * 1) * 2;
  dientich.innerText = w_Cau4 * h_Cau4;
}

// BÀI 5
/**
 * Đầu vào: nhập số
 *
 * Xử lí :
 *  khởi tạo số cần tính;
 *  khởi tạo KQ;
 *  KQ = hang_chuc + hang_donvi;
 *  in ra KQ;
 *
 * Đầu ra: KQ
 */
function cau5() {
  let num_Ex5 = document.getElementById("cau5").value * 1;
  let result_5 = document.getElementById("result_5");

  if (num_Ex5 > 99) {
    return (result_5.innerText = "n < 100");
  }

  result_5.innerText = Math.floor(num_Ex5 / 10) + (num_Ex5 % 10);
}
